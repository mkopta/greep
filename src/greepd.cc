#include <cstdlib>
#include <sstream>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <pqxx/pqxx> // libpqxx

using std::string;

std::ofstream log_handle;

void logger_init(const char *fname) {
	log_handle.open(fname, std::ios::out | std::ios::app);
}

void logger_exit(void) {
	log_handle.close();
}

void logger(const char *msg) {
	if (log_handle.is_open()) {
		log_handle << msg << std::endl;
	} else {
		// at least try stderr
		std::cerr << msg << std::endl;
	}
}

void db_ticket_set_status(pqxx::work &txn, int ticket_id, bool status) {
	txn.exec(
		"update ticket "
		"set status = " + txn.quote(status) +
		"where id = " + txn.quote(ticket_id));
}

void db_ticket_set_status(pqxx::work &txn, int id, bool status, bool paid) {
	txn.exec(
		"update ticket "
		"set status = " + txn.quote(status) + ", "
		"    paid = " + txn.quote(paid) +
		"where id = " + txn.quote(id));
}

pqxx::result db_ticket_get_ids_of_undecided(pqxx::work &txn) {
	return txn.exec(
		"select id "
		"from ticket "
		"where status is null");
}

pqxx::result db_ticket_odd_get_updatable(pqxx::work &txn) {
	return txn.exec(
		"select "
		"  ticket_odd.id, "
		"  odd.status as odd_status "
		"from ticket_odd "
		"join odd "
		"  on ticket_odd.odd_id = odd.id "
		"where odd.status is not null "
		"  and ticket_odd.status is null");
}

void db_ticket_odd_set_status(pqxx::work &txn, int id, bool status) {
	txn.exec(
		"update ticket_odd "
		"set status = " + txn.quote(status) +
		"where id = " + txn.quote(id));
}

pqxx::result db_ticket_odd_get_status_by_ticket_id(pqxx::work &txn,
		int ticket_id) {
	return txn.exec(
		"select status "
		"from ticket_odd "
		"where ticket_id = " + txn.quote(ticket_id));
}

/**
 * For all odds, which status has been already exposed (the sport event already
 * has results), find all updatable ticket_odds of cliets and set their states
 * accordingly.
 * Return true when at leas one ticket_odd has changed.
 */
bool update_ticket_odds(pqxx::work &txn) {
	pqxx::result r = db_ticket_odd_get_updatable(txn);
	bool ticket_odds_updated = r.size() > 0 ? true : false;
	if (ticket_odds_updated) {
		std::ostringstream s;
		s << "Updating " << r.size() << " ticket_odds";
		logger(s.str().c_str());
	}
	for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
		int ticket_odd_id = (*i)["id"].as<int>();
		bool odd_status = (*i)["odd_status"].as<bool>();
		db_ticket_odd_set_status(txn, ticket_odd_id, odd_status);
		std::ostringstream s;
		s << "Updated ticket_odd " << ticket_odd_id << " to status " <<
			odd_status;
		logger(s.str().c_str());
	}
	return ticket_odds_updated;
}

/**
 * For given ticket_id, this function finds all of ticket placed odds
 * and calculates if the ticket should win, fail and if all the odds
 * win.
 * For ticket to win, all odds has to win.
 * For ticket to fail, one odd fail is sufficient.
 * Otherwise, ticket is undecided.
 */
void determine_ticket_status(pqxx::work &txn, int ticket_id,
		bool *ticket_status, bool *all_odds_win) {
	*ticket_status = true;
	*all_odds_win = true;
	pqxx::result ticket_odds =
		db_ticket_odd_get_status_by_ticket_id(txn, ticket_id);
	for (pqxx::result::const_iterator ticket_odd = ticket_odds.begin();
		ticket_odd != ticket_odds.end(); ++ticket_odd) {
		if ((*ticket_odd)[0].is_null()) {
			*all_odds_win = false;
			continue;
		}
		bool ticket_odd_status = (*ticket_odd)[0].as<bool>();
		if (!ticket_odd_status) {
			*ticket_status = false;
			*all_odds_win = false;
		}
	}
}

/**
 * For given ticket determines whether the ticket wins, fails or remains
 * undecided. In case of win, ticket is (mock) paid.
 */
void update_ticket(pqxx::work &txn, pqxx::result::const_iterator &ticket) {
	int ticket_id = (*ticket)["id"].as<int>();
	bool ticket_status, all_odds_win;
	determine_ticket_status(txn, ticket_id, &ticket_status, &all_odds_win);
	if (!ticket_status) {
		db_ticket_set_status(txn, ticket_id, false);
		std::ostringstream s;
		s << "Updated ticket " << ticket_id << " to status " << false;
		logger(s.str().c_str());
	} else if (ticket_status and all_odds_win) {
		// .. there should be payment code
		db_ticket_set_status(txn, ticket_id, true, true);
		std::ostringstream s;
		s << "Updated ticket " << ticket_id << " to status " << true;
		logger(s.str().c_str());
	} /* else ticket remains undecided */
}

/**
 * Finds all undecided tickets and according the ticket_odds states
 * updates tickets states if possible.
 */
void update_tickets(pqxx::work &txn) {
	pqxx::result tickets = db_ticket_get_ids_of_undecided(txn);
	for (pqxx::result::const_iterator ticket = tickets.begin();
			ticket != tickets.end(); ++ticket) {
		update_ticket(txn, ticket);
	}
}

int daemonize(void) {
	pid_t pid, sid;
	pid = fork();
	if (pid < 0) {
		return 1;
	} else if (pid > 0) {
		// parent
		exit(EXIT_SUCCESS);
	}
	umask(0);
	logger_init("/tmp/greepd.log"); // TODO: no error checking
	// TODO: signal handling
	sid = setsid();
	if (sid < 0) {
		logger("Failed to daemonize (setsid())");
		return 1;
	}
	if ((chdir("/")) < 0) {
		logger("Failed to daemonize (chdir to /)");
		return 1;
	}
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	return 0;
}

void main_loop(pqxx::connection &c) {
	do {
		logger("Starting new transaction");
		pqxx::work txn(c); // new transaction
		if (update_ticket_odds(txn))
			update_tickets(txn);
		txn.commit();
		logger("Sleeping");
		sleep(30);
	} while (true);
}

int main(void) {
	try {
		daemonize();
		logger("Greepd started");
		logger("Connecting to database..");
		pqxx::connection c("dbname=greep user=greep");
		main_loop(c);
	} catch (const std::exception &e) {
		logger(string("Exception: ").append(e.what()).c_str());
	}
	logger("Greepd exiting");
	logger_exit();
	return 0;
}
