select * from (
  select
    c1.nick as client_nick,
    count(t.id) as number_of_tickets,
    sum(money) as total_bet,
    /* Je nutno pouzit nejakou agregacni funkci.
     * Sice to tady nedava moc smysl, ale vyhybam se takhle dalsimu vnorenemu
     * selectu.
     * Jde o konstantu takze to nevadi. */
    max(c2.nick) as recomended_nick
  from ticket t
  join client c1
    on t.client_id = c1.id
  left join client c2 /* left protoze ne kazdy klient byl doporucen */
    on c1.client_recomended_id = c2.id
  where
    /* jen skutecni klienti */
    c1.is_test_client != true
    /* pouze tikety solo a ako */
    and (
      t.ticket_type = (select id from ticket_type where name = 'solo')
      or /* zde je trochu nejasne zadani */
      t.ticket_type = (select id from ticket_type where name = 'ako')
    )
    /* pouze nestornovane tikety */
    and t.ticket_status != (select id from ticket_status where name = 'storno')
    /* pouze tikety z aktualniho mesice */
    and extract(month from t.placement_dt) = extract(month from current_date)
  group by c1.nick
  order by
  	number_of_tickets desc,
  	total_bet desc
) sub /* nutno pouzit vnoreny select, protoze nelze pouzit agg fci ve where */
where
  /* celkovy vklad minimalne 500 */
  sub.total_bet > 500
