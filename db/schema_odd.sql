create table if not exists odd (
	id serial primary key,
	bet_id integer not null references bet(id),
	result_id integer not null references result(id),
	status boolean default null
);
