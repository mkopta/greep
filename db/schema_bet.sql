create table if not exists bet (
	id serial primary key,
	sport_id integer not null references sport(id),
	league_id integer not null references league(id)
);
