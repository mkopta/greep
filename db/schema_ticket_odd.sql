create table if not exists ticket_odd (
	id serial primary key,
	ticket_id integer not null references ticket(id),
	odd_id integer not null references odd(id),
	status boolean default null
);
