create table if not exists client (
	id serial primary key,
	nick text not null,
	first_name text,
	second_name text,
	is_test_client boolean not null default false,
	client_recomended_id integer references client(id)
);
