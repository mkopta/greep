insert into client
	(id, nick, first_name, second_name, is_test_client, client_recomended_id)
values
	(3012, 'starman', 'František', 'Hvězda', false, null),
	(3013, 'goblin', 'Josef', 'Zelený', false, 3012),
	(3014, 'ironman', 'Martin', 'Železný', true, 3012);
