create table if not exists ticket (
	id serial primary key,
	client_id integer not null references client(id),
	ticket_type integer not null references ticket_type(id),
	ticket_status integer not null references ticket_status(id),
	money integer not null,
	paid boolean not null default false,
	status boolean default null,
	placement_dt date not null,
	placement_time time not null
);
