create table if not exists ticket_status (
	id serial primary key,
	name text not null
);
